import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'react-uuid';

const Books = (props) => {
    const { books } = props;
    return (
        <ul>
            {books.map((book) =>
                <li key={uuid()}>
                    {book}
                </li>
            )}
        </ul>
    );
}

Books.propTypes = {
    books: PropTypes.array.isRequired,
};

export default Books;