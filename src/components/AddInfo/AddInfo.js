import React from 'react';
import PropTypes from 'prop-types';

const AddInfo = (props) => {
    const { info, handleClick } = props;
    
    return (
            <button onClick={() => handleClick(info)}  className="add__btn">{info}</button>

    );
}

AddInfo.propTypes = {
    handleClick: PropTypes.func,
    info: PropTypes.string,
};

export default AddInfo;
