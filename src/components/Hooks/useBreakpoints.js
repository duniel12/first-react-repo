import { useState, useEffect } from "react";

const useBreakpoints = () => {
    const [data, setData] = useState([]);
    const [windowSize, setWindowSize] = useState([window.innerWidth,window.innerHeight]);
    
    //Resize event to get the width and height of the window
    window.onresize = function(){
        setWindowSize([window.innerWidth,window.innerHeight]);
      }
    const [myWidth, myHeight] = windowSize;

      //Info displayed on the page for the breakpoint data
      const breakPointnfo = (width,height, bkp) => {
        return "Breakpoint: " + bkp+". Width: "+width+" and height: "+height;
      }

    useEffect(() => {
        
            //Breakpoints
            if (myWidth <= 768) {
                setData([breakPointnfo(myWidth, myHeight,"mobile"), "mobile"]);
            } else if (myWidth <= 1024) {
                setData([breakPointnfo(myWidth, myHeight,"tablet"),"tablet"]);
            } else if (myWidth <= 1280) {
                setData([breakPointnfo(myWidth, myHeight,"desktop"),"desktop"]);
            } else {
                setData([breakPointnfo(myWidth, myHeight,"desktop-large"),"desktop-large"]);
            }
        
    }, [myWidth, myHeight]);
    return [data];
};

export default useBreakpoints;