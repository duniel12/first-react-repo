import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'react-uuid';
import { Link } from "react-router-dom";

const Author = (props) => {
    const { author, handleClick } = props;
    
    return (
        <li className="authors__info__list--item" key={uuid()}>
            <Link onClick={() => handleClick(author)} key={author.name} className="authors__btn" to={author.name}>
                {author.name}
            </Link>
        </li>

    );
}

Author.propTypes = {
    author: PropTypes.object,
    handleClick: PropTypes.func,
};

export default Author;
