import Author from "../Author/Author";
import PropTypes from 'prop-types';
import uuid from 'react-uuid';


const AuthorsList = (props) => {
    const {  handleClick, filter, authors } = props;
    //console.log(props);
    return(
        <ul className="authors__info__list">
                {authors.filter(author => author.name.toLocaleLowerCase().includes(filter.toLowerCase())).map((author) => 
                <Author key={uuid()} author={author} handleClick={handleClick}/> 
                )}
            
        </ul>
    );
}

AuthorsList.propTypes = {
    handleClick: PropTypes.func,
    filter: PropTypes.string,
    authors: PropTypes.array,
};

export default AuthorsList;