import React from 'react';
import PropTypes from 'prop-types';
import Books from '../Books/Books';
import uuid from 'react-uuid';
import { useParams } from 'react-router-dom';

const AuthorsInfo = (props) => {
    let { authorId } = useParams();
    const authorsLocal = JSON.parse(localStorage.getItem("authorsLocal"));
    let author = null;
    if(authorsLocal !== null) {
        authorsLocal.forEach((a) => {
            if(a.name.includes(authorId)) {
                author = a;
            }
        });
    }

    //const { name, nationality, books } = props.author;
    //const breakPoint = props.breakPoint[1];
    return (

        <div key={uuid()} className={` authors__info__cnt authors__info__cnt--${author.name}`}>
            <div className="authors__info__cnt--flex">
                <h2 className="authors__info--title">Name</h2>
                <p key={uuid()} className="authors__info__name">{author.name}</p>
                <h2 className="authors__info--title">Nationality</h2>
                <p key={uuid()} className="authors__info__nationality">{author.nationality}</p>
            </div>
            
            {
            //This was with props including the below div
            //(breakPoint === "desktop" || breakPoint === "desktop-large")
        }
            <div className="authors__info__cnt--flex">
                <h2 className="authors__info--title">Books</h2>
                <Books books={author.books} />
            </div>

        </div>
    );
}

AuthorsInfo.propTypes = {
    name: PropTypes.string,
    nationality: PropTypes.string,
    books: PropTypes.array,
};

export default AuthorsInfo;