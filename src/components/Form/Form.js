import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';

const Form = (props) => {
const {author, formType,submitAuthor,handleCloseModal} = props;

let newAuthor = {
    "name": "",
    "nationality": "", "books": []
  };

console.log(author)
if(author) {
    console.log("Entra unde")
    newAuthor.name = author.name;
    newAuthor.nationality = author.nationality;
    newAuthor.books = author.books;
}
    

    const [formAuthorName, setFormAuthorName] = useState( newAuthor.name);
    const [formAuthorNationality, setFormAuthorNationality] = useState( newAuthor.nationality);
    const [formBooksValues, setFormBooksValues] = useState([newAuthor.books]);

    console.log(formAuthorName);

    let handleChangeAuthorDef = (value, e) => {

        if (value === "name") {
        setFormAuthorName( e.target.value);
        console.log("NAME: ",e.target.value)
        } else if (value === "nationality") {
        setFormAuthorNationality( e.target.value);
        console.log("NATIONALITY: ",e.target.value)
        }
    }

    let handleChangeBooks = (i, e) => {
        let newFormValues = [...formBooksValues];
        newFormValues[i][e.target.name] = e.target.value;
        setFormBooksValues(newFormValues);
        console.log(e.target.value, "", e.target.name)
    }
    

    let addFormFields = () => {
        setFormBooksValues([...formBooksValues, { book: "" }])
    }

    let removeFormFields = (i) => {
        let newFormValues = [...formBooksValues];
        newFormValues.splice(i, 1);
        setFormBooksValues(newFormValues)
    }

    //Full submit
    let handleSubmit = (event) => {
        event.preventDefault();
        //Just get the value of book not the object with value inside the array
        let booksArray = formBooksValues.map(v => v.book);
        newAuthor = {
            "name": formAuthorName,
            "nationality": formAuthorNationality, "books": booksArray
          };
        submitAuthor(newAuthor);
        handleCloseModal("no");
    }

    if (formType === "dynamic") {
        return (
            <form onSubmit={(event) => {handleSubmit(event)}} className="form__root">

                <div className="form__author__cnt">
                    
                    <label for="author__name__inp">Name</label>
                    <input type="text" name="author__name__inp" className="form__input" id="author__name__inp" value={formAuthorName || ""} onChange={e => handleChangeAuthorDef("name", e)} />
                    <label for="author__nationality__inp">Nationality</label>
                    <input type="text" name="author__nationality__inp" className="form__input" id="author__nationality__inp" value={formAuthorNationality || ""} onChange={e => handleChangeAuthorDef("nationality", e)} />
                </div>
                <div className="form__books__root">
                    <div>
                    <h3>Books</h3>
                    <button className="form__btn form__btn--add" type="button" onClick={() => addFormFields()}>Add book</button>
                    </div>
                    {formBooksValues.map((element, index) => (
                        <div className="form__books__cnt" key={index}>
                            <label>Book</label>
                            <div>
                            <input type="text" name="book" className="form__input" value={element.book || ""} onChange={e => handleChangeBooks(index, e)} />
                            {
                                index ?
                                    <button type="button" className="form__btn form__btn--remove" onClick={() => removeFormFields(index)}>Remove</button>
                                    : null
                            }
                            </div>
                            
                        </div>
                    ))}
                </div>

                <div className="form__submit">
                    <button className="form__btn submit" type="submit">Submit</button>
                </div>
            </form>
        )
    }

}

Form.propTypes = {
    author: PropTypes.object,
    formType: PropTypes.string.isRequired,
    submitAuthor: PropTypes.func,
    handleCloseModal: PropTypes.func,
}

export default Form;