const Filter = (props) => {
const {setFilter} = props;

    return(
        <div className="filter__root">
            <input onChange={event => setFilter(event.target.value)} className="filter__inp"/>
        </div>
    );
}

export default Filter;