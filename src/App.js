//import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import uuid from 'react-uuid';
import { Outlet } from 'react-router-dom';

import AuthorsList from './components/AuthorsList/AuthorsList';
import AuthorsInfo from './components/AuthorsInfo/AuthorsInfo';
import AddInfo from './components/AddInfo/AddInfo';
import Modal from './components/Modal/Modal';
import Filter from './components/Filter/Filter';
import useModalShow from './components/Hooks/useModalShow';
//Hooks
import useBreakpoints from './components/Hooks/useBreakpoints';

import './components/style.scss';

export const authorsBase = [
  {
    "name": "Stephen King",
    "nationality": "american", "books": ["It", "The Stand", "Carrie"]
  },
  {
    "name": "J.K. Rowling",
    "nationality": "british", "books": ["Harry Potter", "Troubled Blood", "Lethal White"]
  },
  {
    "name": "Amy Tan",
    "nationality": "chinese-american", "books": ["El club de la buena estrella"]
  },
  {
    "name": "Tana French",
    "nationality": "american-irish", "books": ["El silencio del bosque", "En piel ajena", "Faithful Place"]
  },
  {
    "name": "Danzy Senna",
    "nationality": "american", "books": ["New People", "Symptomatic", "Nouveaux visages"]
  }
]

function App() {
  const loadDelay = 1.5;
  const [author, setAuthor] = useState(null);
  const [show, setShow] = useState(true);
  const [showModalAuthor, setShowModalAuthor] = useState("");

  //For filtering
  const [filter, setFilter] = useState("");

  //Debounce hook
  const [results, setResults] = useState([]);
  // Searching status (whether there is pending API request)
  const [isSearching, setIsSearching] = useState(false);

  const [authors, setAuthors] = useState(authorsBase);

  const debouncedSearchTerm = useDebounce(filter, 500);

  const observable = useModalShow();
  const [breakPoint] = useBreakpoints();


  const authorsLocal = JSON.parse(localStorage.getItem("authorsLocal"));
  //console.log(authorsLocal)
  if (authorsLocal !== null) {
    if (!arrayEquals(authorsLocal, authors)) {
      console.log("local storage authors")
      setAuthors(authorsLocal);
    }

  } else {
    localStorage.setItem("authorsLocal",JSON.stringify(authors));
  }
  useEffect(
    () => {


      if (debouncedSearchTerm) {
        console.log("debounce ")
        setIsSearching(true);
        //This is for the api search function
        //searchCharacters(debouncedSearchTerm).then((results) => {setIsSearching(false);setResults(results);});
      } else {
        setResults([]);
        setIsSearching(false);
      }
      let timer1 = setTimeout(() => setShow(false), loadDelay * 1000);
      return () => {
        clearTimeout(timer1);
      };

    },
    [debouncedSearchTerm]
  );

  //Shows info of authors
  const handleClick = (pauthor) => {
    if (pauthor === author) {
      setAuthor(null);
    } else {
      setAuthor(pauthor);
    }

  }

  const FilterChange = (info) => {
    setFilter(useDebounce(info, 1000));
  }

  //Determines if its a new author click or an edit button
  //Info is the parameter for add or edit author
  const handleAuthor = (info) => {
    console.log(info)
    if (info === "Add author") {
      setShowModalAuthor("Add author");
    } else if (info === "Edit author") {
      setShowModalAuthor("Edit author");
    }
  }

  const handleCloseModal = (info) => {
    setShowModalAuthor(info);
  }

  const submitAuthor = (author, modal) => {
    let newAuthors = authors;
    newAuthors.push(author);
    localStorage.setItem("authorsLocal", JSON.stringify(newAuthors));
    console.log("Authors en local: ", newAuthors);
    setShowModalAuthor(modal);
  }

  return (
    <div>
      {show && <Modal addClose={false} show={show} />}

      {
        //Show modal of add author
        showModalAuthor === "Add author" &&
        <Modal addClose={true} show={false} title={"Add author"} handleCloseModal={handleCloseModal} submitAuthor={submitAuthor} />
      }
      {
        //Show modal of edit author
        showModalAuthor === "Edit author" &&
        <Modal addClose={true} show={false} title={"Edit author"} handleCloseModal={handleCloseModal} submitAuthor={submitAuthor} />
      }
      <div className="authors__root">
        <div className="authors__breakpoints">
          {breakPoint.length > 0 && breakPoint[0]}
        </div>

        <div className="authors__info__btns">
          <h2 className="authors__title">Authors</h2>
          <AddInfo info={"Add author"} handleClick={() => handleAuthor("Add author")} />
          <Filter setFilter={setFilter} />
          <AuthorsList handleClick={handleClick} authors={authors} filter={filter} />
        </div>

        <div className="authors__info__root">
          { 
          //<AuthorsInfo key={uuid()} breakPoint={breakPoint} author={author} />
          }
          <Outlet />


        </div>
      </div>
    </div>



  );
}

// Hook
function useDebounce(value, delay) {
  // debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );
  return debouncedValue;
}

const arrayEquals = function (value, other) {
  var type = Object.prototype.toString.call(value);

  if (type !== Object.prototype.toString.call(other)) return false;


  if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

  var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
  var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
  if (valueLen !== otherLen) return false;

  return true;

};

export default App;
